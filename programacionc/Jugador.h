
#ifndef JUGADOR_H
#define JUGADOR_H

#include cadena (string)


/**
  * class Jugador
  * 
  */

class Jugador
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Jugador();

  /**
   * Empty Destructor
   */
  virtual ~Jugador();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Accedente del Nombre del jugador
   * @return cadena (string)
   */
  cadena (string) nombre()
  {
  }


  /**
   * Mutador de nombre
   * @param  nuevo_nm
   */
  void nombre(cadena (string) nuevo_nm)
  {
    this->nombre_=nuevo_nm;
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  cadena (string) nombre_;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of nombre_
   * @param value the new value of nombre_
   */
  void setNombre_(cadena (string) value)
  {
    nombre_ = value;
  }

  /**
   * Get the value of nombre_
   * @return the value of nombre_
   */
  cadena (string) getNombre_()
  {
    return nombre_;
  }

  void initAttributes();

};

#endif // JUGADOR_H
