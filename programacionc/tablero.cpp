#include "tablero.h"

#include <stdio.h>
#include <stdlib.h>

#define SPCS 4


void guardar_cursor() {
    printf("\x1B[s"); // Guardar Cursor
}

void poner_cursor(int fila, int col) {
    printf("\x1B[%i;%iH", fila, col);   // Mover cursor
}

void restablecer_cursor(){
    printf("\x1B[u");
}

void
pintar_cols(const char *normal, const char *intersecc) {
    for (int col=0; col<3; col++){
        printf ("%s%s%s", normal, normal, normal);

        if (col != 2)
            printf ("%s", intersecc);
    }
    printf("\n");
}

void
pintar_marco () {

    printf ("\x1B[96m");
    for (int fila=0; fila<3; fila++){
        
        // Fila superior de espacios
        poner_cursor(Y0T + SPCS * fila + 0, X0T);
        pintar_cols("  ", "│");

        // Fila de los números
        poner_cursor(Y0T + SPCS * fila + 1, X0T);
        pintar_cols("  ", "│");

        // Fila inferior de espacios
        poner_cursor(Y0T + SPCS * fila + 2, X0T);
        pintar_cols("  ", "│");

        // Fila de separadores
        if (fila != 2) {
            poner_cursor(Y0T + SPCS * fila + 3, X0T);
            pintar_cols("──", "┼");
        }
    }

    
    restablecer_cursor();
    printf ("\x1B[0m\n");
}

void
tablero_grafico(int fichas[JUGADORES][FICHAS][DIMENS]) {
    pintar_marco();

    // Pintar fichas
    const char *TFICHA[] = { 
        "\x1B[36mO\x1B[0m", 
        "\x1B[39mX\x1B[0m"
    };

    for (int jug=j0; jug<JUGADORES; jug++)
        for (int ficha=0; ficha<NFICHAS; ficha++){
            poner_cursor(
                Y0T + 1 + (SPCS - 1) * fichas[jug][ficha][FILA], 
                X0T + 1 + (SPCS - 1 + 2) * fichas[jug][ficha][COL]);
            printf("%s", TFICHA[jug]);
        }
    
}



void
tablero_debug(int fichas[JUGADORES][FICHAS][DIMENS]){
    printf ("TABLERO\n");
    printf ("=======\n\n\n");

    for (int j=0; j<JUGADORES; j++){
        printf("Jugador %i\n", j + 1);
        for (int i=0; i<FICHAS; i++)
            if (!esta_en_casa(fichas[j][i]))
                printf("F%i: [%i, %i]\n", i+1, fichas[j][i][FILA], fichas[j][i][COL]);

        printf("\n");
    }
}