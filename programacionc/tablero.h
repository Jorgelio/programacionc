#ifndef __TABLERO_H__
#define __TABLERO_H__

#include "general.h"
#include "interfaz.h"
#include "ficha.h"

#define X0T 50
#define Y0T 10

#ifdef __cplusplus
extern "C" {
#endif

void guardar_cursor();
void restablecer_cursor();
void poner_cursor(int fila, int col);

void tablero_debug  (int fichas[JUGADORES][FICHAS][DIMENS]);
void tablero_grafico(int fichas[JUGADORES][FICHAS][DIMENS]);

#ifdef __cplusplus	
}
#endif

#endif